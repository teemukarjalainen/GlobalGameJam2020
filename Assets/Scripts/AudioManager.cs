﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;     //Allows other scripts to call functions from SoundManager.             

    public AudioSource musicSource;
    private float lowPitchRange = 0.5f;
    private float highPitchRange = 2.5f;

    public static AudioClip hammerDown, ufoShooter, speechBubble, extinguisher, bearAppears, ufoAppears, fireAppears;
    AudioClip gameMusic;
    // Use this for initialization
    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
        }
        //If instance already exists:
        else if (instance != this)
        {
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
        }
        musicSource = GetComponent<AudioSource>();

        hammerDown = Resources.Load<AudioClip>("Audio/hammer");
        ufoShooter = Resources.Load<AudioClip>("Audio/laser gun");
        speechBubble = Resources.Load<AudioClip>("Audio/perkele cut louder");
        extinguisher = Resources.Load<AudioClip>("Audio/extinguisher");
        bearAppears = Resources.Load<AudioClip>("Audio/Bear_Growl01");
        ufoAppears = Resources.Load<AudioClip>("Audio/laser beam 1");
        fireAppears = Resources.Load<AudioClip>("Audio/Campfire_Loop_Large");
        gameMusic = Resources.Load<AudioClip>("Audio/Wood Oxidizer - Peetu Nuottajärvi");
        PlayLoopMusic(gameMusic);
    }

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayLoopMusic(AudioClip clip)
    {
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        //Play the clip.
        musicSource.loop = true;
        musicSource.Play();
    }
    public void PlayMusicOnce(AudioClip clip)
    {
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        //Play the clip.
        musicSource.Play();
    }

    //Used to play single sound clips from other sources than player.
    public void PlaySingle(AudioClip clip, AudioSource src)
    {
        if (clip != null)
        {
            if (src.loop == true)
            {
                src.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            src.clip = clip;

            //Play the clip.
            src.Play();
        }
    }

    //Used to play single sound clips.
    public void PlayLoop(AudioClip clip, AudioSource src)
    {
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        src.clip = clip;

        //Play the clip.
        src.loop = true;
        src.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(AudioClip clip, AudioSource src)
    {

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        src.pitch = randomPitch;
        src.clip = clip;

        //Play the clip.
        src.Play();
    }

}
