﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI pointsText;

    public long score;
    public bool gameOver;

    private float nextPointAddTime;
    private float executionTime = 1.0f;

    //Awake is always called before any Start functions
    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
            if (Time.timeSinceLevelLoad > nextPointAddTime)
            {
                nextPointAddTime += executionTime;

                score += 10;
                pointsText.SetText(score.ToString());
            }
        }
    }

    public void setGameOver(bool gameOver)
    {
        this.gameOver = gameOver;
        if(gameOver)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public bool getGameOver()
    {
        return gameOver;
    }

    
}
