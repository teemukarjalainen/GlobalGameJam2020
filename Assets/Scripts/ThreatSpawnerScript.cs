﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coordinates {
        public static Vector3[] coordinates = {
        new Vector3(-9, 4.5f,  0),
        new Vector3(-5, 8f, 0),
        new Vector3(-9, 13, 0),
        new Vector3(9,  4.5f,  0),
        new Vector3(5,  8f, 0),
        new Vector3(9,  13, 0),
    };
}
public class ThreatSpawnerScript : MonoBehaviour
{
    public List<GameObject> threats = new List<GameObject>();
    public GameController gameController;
    private float waitTime = 3.0f;
    private float timer = 0.0f;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!gameController.getGameOver())
        {
            timer += Time.deltaTime;

            if (timer > waitTime)
            {
                spawnThreat();

            }
        }
        if (Time.timeSinceLevelLoad > 30 && waitTime > 2.75f)
        {
            waitTime = 2.75f;
        }
        if (Time.timeSinceLevelLoad > 60 && waitTime > 2.5f)
        {
            waitTime = 2.5f;
        }
    }

    void spawnThreat()
    {
        GameObject threat;
        timer = timer - waitTime;
        int rng = rollD6Int();
        //Debug.Log(rng);

        threat = threats[rng];
        
        if (!GameObject.Find(threat.name + "(Clone)"))
        {
            Instantiate(threat);
        }
    }

    public int rollD6Int()
    {
        int value = Random.Range(1, 60);

        if (value >= 1 && value <= 10)
        {
            value = 0;
        }
        else if (value >= 11 && value <= 20)
        {
            value = 1;
        }
        else if (value >= 21 && value <= 30)
        {
            value = 2;
        }
        else if (value >= 31 && value <= 40)
        {
            value = 3;
        }
        else if (value >= 41 && value <= 50)
        {
            value = 4;
        }
        else if (value >= 51 && value <= 60)
        {
            value = 5;
        }

        return value;
    }
}
