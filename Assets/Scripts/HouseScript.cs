﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HouseScript : MonoBehaviour
{

    private List<RoomScript> rooms = new List<RoomScript>();
    public Slider healthSlider;
    public float healthMax;
    public float health;
    public GameController gameController;
    void Awake()
    {
        GameObject[] roomObjects = GameObject.FindGameObjectsWithTag(Constants.TAG_ROOM);
        foreach (GameObject obj in roomObjects)
        {
            try
            {
                RoomScript room = obj.GetComponent<RoomScript>();
                Debug.Log("obj: " + obj + " room: " + room);
                rooms.Add(room);
            } catch (System.Exception e)
            {
                Debug.LogWarning(e);
            }
        }

        foreach (RoomScript room in rooms)
        {
            healthMax += room.getHealth();
        }


        healthSlider.minValue = 0;
        healthSlider.maxValue = healthMax;
        healthSlider.value = healthMax;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameController.getGameOver())
        {
            /*
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                rooms[0].dealDamage(10);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                rooms[0].addHealth(10);
            }
            */


            float currentHealth = 0;
            foreach (RoomScript room in rooms)
            {
                float roomHealth = room.getHealth();
                if(roomHealth < 0)
                {
                    currentHealth -= Mathf.Abs(roomHealth);
                } else
                {
                    currentHealth += roomHealth;
                }
            }

            setHealth(currentHealth);
        }
    }

    private void setHealth(float healthToSet)
    {
        if (healthToSet != health)
        {
            health = healthToSet;
            if (health > healthMax)
            {
                health = healthMax;
            }

            healthSlider.value = health;

            if (health <= 0)
            {
                health = 0;
                PlayerPrefs.SetFloat("Score", gameController.score);
                gameController.setGameOver(true);
            }
            //Debug.Log("health: " + health);
        }
    }
}
