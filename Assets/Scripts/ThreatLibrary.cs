﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Threat
{
    public Threat(string repairId, Sprite sprite, int coordinates)
    {
        this.repairId = repairId; //Make sure that this is same as the items repairid
        this.sprite = sprite;
        this.coordinates = coordinates;
    }    
    
    public Threat(string repairId, int coordinates)
    {
        this.repairId = repairId; //Make sure that this is same as the items repairid
        this.coordinates = coordinates;
    }
    public string repairId;
    public Sprite sprite;
    public int coordinates;

}
public class ThreatLibrary : MonoBehaviour
{
    public Threat getFire()
    {
        return new Threat("Fire", 0);
    }


}
