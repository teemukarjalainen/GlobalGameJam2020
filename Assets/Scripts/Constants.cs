﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    // Prevent instantiation
    private Constants(){}

    public static string TAG_LADDER = "Ladder";
    public static string TAG_ITEM = "Item";
    public static string TAG_THREAT = "Threat";
    public static string TAG_ROOM = "Room";

    public static string PLAYER_ONE = "PlayerOne";
    public static string PLAYER_TWO = "PlayerTwo";

    // Sprite paths
    public static string SPRITES_FOLDER_PATH = "Sprites/";

    // Room backgrounds (They need additional info for half and broken images (PATH + 1 or 2) OR if roof PATH + L/R
    public static string TALO_ATLAS_PATH = SPRITES_FOLDER_PATH + "talo_atlas";
    public static string FULL_ROOM_SPRITE = "RoomHealthy";
    public static string HALF_ROOM_SPRITE = "RoomHalf";
    public static string BAD_ROOM_SPRITE = "RoomBroken";

    public static string FULL_ROOF_SPRITE = "RoofHealthy";
    public static string HALF_ROOF_SPRITE = "RoofHalf";
    public static string BAD_ROOF_SPRITE = "RoofBroken";


    public static string NAME_HAMMER = "Hammer";
}
