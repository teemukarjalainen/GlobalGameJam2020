﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionScript : MonoBehaviour
{

    public bool hasItem, throwItem, touchingItem = false;
    public GameObject item, threat, room;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(GetComponent<MovementScript>().playerPrefix + "Action")) 
        {

            if (!hasItem)
            {
                //Pickup if collided with thing
                if (item)
                {
                    //Pickup the item
                    item.transform.position = transform.position + new Vector3(1, 1, 0);

                    hasItem = true;
                }
            } else if (hasItem && !threat)
            {
                //Throw
                throwItem = true;
            }
            else if (hasItem && threat)
            {
                //Destroy threat
                if (threat.GetComponent<ThreatScript>().repairId == item.GetComponent<ItemScript>().repairId)
                {
                    playItemSound(item.GetComponent<ItemScript>());
                    Destroy(threat);
                }
            }
        }
        if (hasItem)
        {
            Transform child = transform.GetChild(0);
            item.transform.position = new Vector3(child.position.x, child.position.y, child.position.z);
        }
    }
    //Ima baad coodeer duu duduu dududu
    private void playItemSound(ItemScript itemScript)
    {
        AudioSource src = GetComponent<AudioSource>();
        AudioManager.instance.PlaySingle(itemScript.repairId == "Fire" ? AudioManager.extinguisher : itemScript.repairId == "Ufo" ? AudioManager.ufoShooter : AudioManager.speechBubble, src);
    }
    void FixedUpdate()
    {
        if(throwItem)
        {
            yeet();
            throwItem = false;
        }
    }
    public void yeet(){
        hasItem = false;
        Vector3 directionToThrow = GetComponent<SpriteRenderer>().flipX ? new Vector3(15, 5, 0) : new Vector3(-15, 5, 0);

        item.GetComponent<Rigidbody>().AddForce(directionToThrow, ForceMode.VelocityChange);
        if(item.name == Constants.NAME_HAMMER)
        {
            AudioSource src = GetComponent<AudioSource>();
            AudioManager.instance.PlaySingle(AudioManager.hammerDown, src);
            room.GetComponent<RoomScript>().addHealth(25);
        }
        item = null;

    }

    public void OnTriggerStay(Collider other)
    {
        if (other.tag == Constants.TAG_ITEM && !item)
        {
            //Set the collided item as next pickup item
            item = other.gameObject;
        } 
        if (other.tag == Constants.TAG_THREAT)
        {
            //Set the threat
            threat = other.gameObject;
        }
        if (other.tag == Constants.TAG_ROOM)
        {
            //Set the room
            room = other.gameObject;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        threat = other.gameObject == threat ? null : threat;
        if(!hasItem)
        {
            item = null;
        }
    }
}
