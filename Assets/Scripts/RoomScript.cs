﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomScript : MonoBehaviour
{
    private List<Sprite> sprites;
    private SpriteRenderer renderer;
    private float healthMax;
    private float healthMin = -500;
    public float health = 100f;
    public bool isRoof;
    public bool isLeft;

    public bool hasThreat;
    private ThreatScript currentThreat;

    private float nextDamageTime = 0.0f;
    private float executionTime = 3.0f;


    private enum RoomState
    {
        FULL,
        HALF,
        BAD
    }
    private RoomState currentState = RoomState.FULL;

    // Start is called before the first frame update
    void Start()
    {
        sprites = new List<Sprite>(Resources.LoadAll<Sprite>(Constants.TALO_ATLAS_PATH));
        renderer = GetComponent<SpriteRenderer>();
        healthMax = health;
    }

    void Update()
    {
    
    }

    public void dealDamage(float damage)
    {
        health -= damage;
        if (health <= healthMin)
        {
            health = healthMin;
        }
        handleSprite();
    }

    public void addHealth(float health)
    {
        this.health += health;

        if (this.health > healthMax)
        {
            this.health = healthMax;
        }
        handleSprite();
    }

    public float getHealth()
    {
        return health;
    }

    public void setHealth(float health)
    {
        this.health = health;

        if (this.health > healthMax)
        {
            this.health = healthMax;
        }
        handleSprite();
    }


    void OnTriggerStay(Collider trigger)
    {
        if (trigger.tag == Constants.TAG_THREAT)
        {
            if (currentThreat == null)
            {
                currentThreat = trigger.gameObject.GetComponent<ThreatScript>();
            }

            if(currentThreat != null)
            {
                if (Time.timeSinceLevelLoad > nextDamageTime)
                {
                    nextDamageTime += executionTime;
                    dealDamage(currentThreat.getDamage());
                }
            }
            //Debug.Log("onTriggerStay: " + currentThreat);
        }
    }

    private void handleSprite()
    {
        RoomState prevState = currentState;

        if (health >= healthMax)
        {
            currentState = RoomState.FULL;
        }
        else if (health <= healthMax / 4)
        {
            currentState = RoomState.BAD;
        } else
        {
            currentState = RoomState.HALF;
        }


        if (currentState != prevState)
        {
            string number = Mathf.Round(Random.Range(0.51f, 2.499f)).ToString();
            switch (currentState)
            {
                case RoomState.FULL:
                    if (isRoof)
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.FULL_ROOF_SPRITE + (isLeft ? "L" : "R"));
                    }
                    else
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.FULL_ROOM_SPRITE);
                    }
                    break;

                case RoomState.HALF:
                    if (isRoof)
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.HALF_ROOF_SPRITE + (isLeft ? "L" : "R"));
                    }
                    else
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.HALF_ROOM_SPRITE + number);
                    }
                    break;

                case RoomState.BAD:
                    if (isRoof)
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.BAD_ROOF_SPRITE + (isLeft ? "L" : "R"));
                    }
                    else
                    {
                        renderer.sprite = sprites.Single(s => s.name == Constants.BAD_ROOM_SPRITE + number);
                    }
                    break;

            }
        }
    }
}
