﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    public bool playerOne = true;
    public float speed;

    public float jumpSpeed = 69.0f;
    public float gravity = 20.0f;

    public bool allowClimb;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 moveDirectionVert = Vector3.zero;

    public SpriteRenderer spriteRenderer;
    private Rigidbody rigidbody;

    public string playerPrefix = "PlayerOne";

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        playerPrefix = playerOne ? Constants.PLAYER_ONE : Constants.PLAYER_TWO;
    }

    void FixedUpdate()
    {
        move();
    }

    private void move()
    {
        moveDirection = new Vector3(Input.GetAxis(playerPrefix + "Horizontal"), 0.0f, 0.0f);
        if (allowClimb)
        {
            moveDirection += new Vector3(0.0f, Input.GetAxis(playerPrefix + "Vertical"), 0.0f);
        }

        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection = moveDirection * speed;
        if(moveDirection.x < 0)
        {
            spriteRenderer.flipX = false;
        } else if (moveDirection.x > 0) 
        {
            spriteRenderer.flipX = true;
        }
        transform.position += moveDirection * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider trigger)
    {
        if (trigger.tag == Constants.TAG_LADDER)
        {
            allowClimb = true;
            rigidbody.useGravity = false;
        }
    }

    private void OnTriggerExit(Collider trigger)
    {
        if (trigger.tag == Constants.TAG_LADDER)
        {
            allowClimb = false;
            rigidbody.useGravity = true;
        }
    }

}
