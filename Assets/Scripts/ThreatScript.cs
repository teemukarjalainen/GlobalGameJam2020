﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreatScript : MonoBehaviour
{
    public string repairId = "Fire";
    public int coordinate = 0;
    public bool isRightSide = true;

    private int damage = 7;
    
    // Start is called before the first frame update
    void Start()
    {
        coordinate = isRightSide ? coordinate : coordinate + 3;
        transform.position = Coordinates.coordinates[coordinate];
        playThreatSound();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeSinceLevelLoad > 30 && damage < 15)
        {
            damage = 10;
        }
        else if (Time.timeSinceLevelLoad > 60 && damage < 15)
        {
            damage = 13;
        }
    }

    public int getDamage()
    {
        return damage;
    }

    //Bad code but w/e
    private void playThreatSound()
    {
        AudioSource src = GetComponent<AudioSource>();
        AudioManager.instance.PlaySingle(repairId == "Fire" ? AudioManager.fireAppears : repairId == "Ufo" ? AudioManager.ufoAppears : AudioManager.bearAppears, src);
    }
}
